import Vue from 'vue'
import Router from 'vue-router'

// Import page components
import Products from '../components/pages/Products'
import Home from '../components/pages/Home'
import About from '../components/pages/About'

Vue.use(Router)

let routes = [
  {path: '/', component: Home},
  {path: '/products', component: Products},
  {path: '/about', component: About}
]

export default new Router({
  routes,
  linkExactActiveClass: 'active'
})
